all : testlib testlib++ testlib.pas
install : testlib_install testlib++_install testlib.pas_install

testlib :
	$(MAKE) -C testlib all

testlib++ :
	$(MAKE) -C testlib++ all

testlib.pas :
	$(MAKE) -C testlib.pas all

testlib_install :
	$(MAKE) -C testlib install

testlib++_install :
	$(MAKE) -C testlib++ install

testlib.pas_install :
	$(MAKE) -C testlib.pas install

.PHONY: testlib testlib++ testlib.pas testlib_install testlib++_install testlib.pas_install
